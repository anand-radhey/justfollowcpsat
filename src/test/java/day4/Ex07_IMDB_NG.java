package day4;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex07_IMDB_NG {

	WebDriver driver = null;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
	}

	
	
	
	
	@Test
	public void testIMDB1() {
		String movie = "Avengers";
		String expStar = "Chris Evans";  
		String expDir = "Joss Whedon";  

		driver.get("https://www.imdb.com/");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]")).sendKeys(movie);
		driver.findElement(By.xpath("//*[@id=\"suggestion-search-button\"]")).click();

		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();

		//*[@class="inline" and contains(text(),'Star'  )]/following-sibling::*    All Stars' siblings

		By byStar  = By.xpath("//*[@class=\"inline\" and contains(text(),'Star'  )]/following-sibling::*");
		List<WebElement>   starList   = driver.findElements(byStar);


		for (WebElement   ele   : starList) {
			String actStar = ele.getText();
			if (actStar.contains(expStar)) {
				System.out.println("Found Star : "+ actStar );
				break;

			}
		}

		By byDir  = By.xpath("//*[@class=\"inline\" and contains(text(),'Dir'  )]/following-sibling::*");
		List<WebElement>   dirList   = driver.findElements(byDir);

		for (WebElement   ele1   : dirList) {
			String actDir = ele1.getText();
			if (actDir.contains(expDir)) {
				System.out.println("Found Dir : "+ actDir );
				break;

			}
		}

	}

	
	

	@Test
	public void testIMDB2() {
		String movie = "Raazi"; 
		String expStar = "Vicky";  
		String expDir = "Anuraag";  

		driver.get("https://www.imdb.com/");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]")).sendKeys(movie);
		driver.findElement(By.xpath("//*[@id=\"suggestion-search-button\"]")).click();

		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();

		//*[@class="inline" and contains(text(),'Star'  )]/following-sibling::*    All Stars' siblings

		By byStar  = By.xpath("//*[@class=\"inline\" and contains(text(),'Star'  )]/following-sibling::*");
		List<WebElement>   starList   = driver.findElements(byStar);

		boolean flagStar = false;
		for (WebElement   ele   : starList) {
			String actStar = ele.getText();
			if (actStar.contains(expStar)) {
				System.out.println("Found Star : "+ actStar );
				flagStar = true;
				break;

			}
			
		}
		
		Assert.assertEquals(flagStar, true, "Star Matching Not done");
		
		

		By byDir  = By.xpath("//*[@class=\"inline\" and contains(text(),'Dir'  )]/following-sibling::*");
		List<WebElement>   dirList   = driver.findElements(byDir);

		boolean flagDir = false;
		for (WebElement   ele1   : dirList) {
			String actDir = ele1.getText();
			if (actDir.contains(expDir)) {
				System.out.println("Found Dir : "+ actDir );
				flagDir = true;
				break;

			}
		}
		
		Assert.assertEquals(flagDir, true, "Director Matching Not done");

	}
	

	@AfterTest
	public void afterTest() {

		driver.quit();

	}

}
