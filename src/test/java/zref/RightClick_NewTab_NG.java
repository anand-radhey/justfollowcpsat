package zref;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class RightClick_NewTab_NG {

	@Test
	public void method1() {

		//WebDriver driver = new FirefoxDriver();
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.edureka.co");
		System.out.println(driver.getTitle());
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.partialLinkText("Corporate Training"));

		action.contextClick(element).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();

		Set<String> winid = driver.getWindowHandles();
		System.out.println("No. of win handles : "+ winid.size());
		Iterator<String> iter = winid.iterator();
		iter.next();

		String tab = iter.next();
		driver.switchTo().window(tab);
		System.out.println(driver.getTitle());

		driver.quit();
	}
}