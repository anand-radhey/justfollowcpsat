package zref;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
//import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class RightClick_NewTab2_NG2 {

	@Test
	public void method1() {

		//WebDriver driver = new FirefoxDriver();
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome"); //new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://www.way2testing.com/");
		System.out.println(driver.getTitle());
		
		//*[@id="post-body-751561208295527719"]/center/div[1]/table/tbody/tr[1]/td[4]/a/img
		
		WebElement weButton = driver.findElement(By.xpath("//*[@id=\"post-body-751561208295527719\"]/center/div[1]/table/tbody/tr[1]/td[4]/a/img"));
		
		
		
		Actions action = new Actions(driver);
		
		
		action.contextClick(weButton).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		
		Set<String> winid = driver.getWindowHandles();
		System.out.println("No. of win handles : "+ winid.size());
		Iterator<String> iter = winid.iterator();
		iter.next();

		String tab = iter.next();
		driver.switchTo().window(tab);
		System.out.println(driver.getTitle());

		driver.quit();
	}
}