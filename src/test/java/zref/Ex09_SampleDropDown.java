package zref;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Ex09_SampleDropDown {

	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		
		driver.get("file:///C:/ZVA/CPSAT/mavenCPSATonlineAPR/src/test/resources/data/dropdown.html");
		//     /html/body/form/select
		//       /html/body/form/select
		WebElement weselect = driver.findElement(By.xpath("/html/body/form/select"));
		
		//List<WebElement> listweselect = driver.findElements(By.xpath("/html/body/form/select/"));
		
		Select selectObject = new Select(weselect);

		List<WebElement>   listOfOptions	 = selectObject.getOptions();
		
		
		for(WebElement ele : listOfOptions) {
			System.out.println(ele.getText());
			
		}
		
try {
	Thread.sleep(2000);
} catch (InterruptedException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
		
		selectObject.selectByVisibleText("CP-AAT");
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		selectObject.selectByVisibleText("CP-MLDS");
		
		
		driver.quit();

	}

}
