package day5;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
//import org.testng.annotations.Test;

public class Ex09_SampleDropDown {
	WebDriver driver = null;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
	}


  @Test
  public void f() {
	  
	  driver.get("file:///C:/ZVA/CPSAT/mavenCPSATonlineJUL/src/test/resources/data/dropdown.html");
	  WebElement wesel = driver.findElement(By.xpath("/html/body/form/select"));
	  
	  Select selectObj = new Select(wesel);
	  
	  
	  List<WebElement> lstOptions = selectObj.getOptions();
	  
	  
	  for (WebElement ele : lstOptions) {
		  System.out.println(ele.getText());
		  
	  }
	  
	  
	  selectObj.selectByValue("71");
	  
	  try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  selectObj.selectByVisibleText("CP-MLDS");
	  
	  
	  
	  
	  
  }
  
  @AfterTest
  public void afterTest() {
	  driver.quit();
  }
}
