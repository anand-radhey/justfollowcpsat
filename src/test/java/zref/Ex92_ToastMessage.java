package zref;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ex92_ToastMessage {

	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://demo.actitime.com/");
		//*[@id="username"]                  //username
		driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys("admin");
		
		//*[@id="loginFormContainer"]/tbody/tr[1]/td/table/tbody/tr[2]/td/input     //password
		driver.findElement(By.xpath("//*[@id=\"loginFormContainer\"]/tbody/tr[1]/td/table/tbody/tr[2]/td/input")).sendKeys("manager");
		
		//*[@id="loginButton"]/div        //Login Button
		driver.findElement(By.xpath("//*[@id=\"loginButton\"]/div")).click();
		
		
		
		//*[@id="container_tasks"]
		driver.findElement(By.xpath("//*[@id=\"container_tasks\"]")).click();
		
		//*[@id="taskManagementPage"]/div[1]/div[1]/div[1]/div[1]/div[3]/div/div[2]    //task tab
		WebDriverWait wait = new WebDriverWait(driver,  10);
		
		By bytasktab = By.xpath("//*[@id=\"taskManagementPage\"]/div[1]/div[1]/div[1]/div[1]/div[3]/div/div[2]");
		wait.until(ExpectedConditions.elementToBeClickable(bytasktab));
		
	
		driver.findElement(By.xpath("//*[@id=\"taskManagementPage\"]/div[1]/div[1]/div[1]/div[1]/div[3]/div/div[2]")).click();
		
		//      /html/body/div[22]/div[1]     //New Customer item
		driver.findElement(By.xpath("/html/body/div[22]/div[1]")).click();
		
		//Enter Customer Name input
		//*[@id="customerLightBox_content"]/div[2]/div[1]/div/div[1]/div[1]/input   
		
		driver.findElement(By.xpath("//*[@id=\"customerLightBox_content\"]/div[2]/div[1]/div/div[1]/div[1]/input")).sendKeys("Anand25");
		
		//Description
		//*[@id="customerLightBox_content"]/div[2]/div[1]/div/div[1]/div[2]/div[2]/div/div[1]/div[1]/textarea
		driver.findElement(By.xpath("//*[@id=\"customerLightBox_content\"]/div[2]/div[1]/div/div[1]/div[2]/div[2]/div/div[1]/div[1]/textarea")).sendKeys("Desc_Anand");
		
		//Create Customer
		//*[@id="customerLightBox_content"]/div[3]/div[2]/div[1]/div/div[1]
		driver.findElement(By.xpath("//*[@id=\"customerLightBox_content\"]/div[3]/div[2]/div[1]/div/div[1]")).click();
		
		//Toast   
		//        /html/body/div[13]/div/div/span[1]
				String toastMsg = driver.findElement(By.xpath("/html/body/div[13]/div/div/span[1]")).getText();
				System.out.println(toastMsg);
				
		

	}

}
