package day3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Ex03_AnnaUniv {

	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome", false);
		
		
		driver.get("https://www.annauniv.edu/department/index.php");
		
		By byvar1 = By.xpath("//*[@id=\"link3\"]/strong");
		WebElement wecivil = driver.findElement(byvar1);

		WebElement weiom = driver.findElement(By.xpath("//*[@id=\"menuItemHilite32\"]"));
		
		//WebElement weiom = driver.findElement(By.id("menuItemHilite32"));
		
		Actions action = new Actions(driver);
		
		action.moveToElement(wecivil).moveToElement(weiom).click().perform();
		
		
		//action.moveToElement(weiom).click().perform();
		
		System.out.println("Page Title for IOM : "+ driver.getTitle());
		
		driver.quit();
		
		
		
		
		
		
		
		
		

	}

}
