package day3;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import junit.framework.Assert;

public class Ex_Google_JU {
	
	WebDriver driver = null;
	
	

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.manage().window().maximize();
	}

	@After
	public void tearDown() throws Exception {
		driver.close();
	}

	@Test
	public void test() {
		
		driver.get("http://www.google.com");
		String actual = driver.getTitle();

		System.out.println("PAGE TITLE is : " +  actual);
		
		//Assert.assertEquals("GOOGLE", actual);

	}

}
