package day4;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex07_IMDB_DP_NG {

	WebDriver driver = null;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
	}

	

	

	  @DataProvider
	  public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
		 /* 
		  String[][] data = {{ "Avengers","Joss Whedon","Chris Evans","4th"   },
				  			 { "Raazi","Meghna","Vicky","4th"   }    }; 
		  */
		  
		  String[][] data = utils.XLDataReaders.getExcelData("C:\\ZVA\\CPSAT\\mavenCPSATonlineJUL\\src\\test\\resources\\data\\imdbdata.xlsx", "data");
		  	  
		  
		  return data;
		  
		  
		  
		  
	  }	

	
	  
  @Test(dataProvider = "dp")
	public void testIMDB1(String v1, String v2, String v3) {
		String movie = v1;   //"Avengers";
		String expStar = v2;   // "Chris Evans";  
		String expDir = v3;   // "Joss Whedon";  

		driver.get("https://www.imdb.com/");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]")).sendKeys(movie);
		driver.findElement(By.xpath("//*[@id=\"suggestion-search-button\"]")).click();

		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();

		//*[@class="inline" and contains(text(),'Star'  )]/following-sibling::*    All Stars' siblings

		By byStar  = By.xpath("//*[@class=\"inline\" and contains(text(),'Star'  )]/following-sibling::*");
		List<WebElement>   starList   = driver.findElements(byStar);


		for (WebElement   ele   : starList) {
			String actStar = ele.getText();
			if (actStar.contains(expStar)) {
				System.out.println("Found Star : "+ actStar );
				break;

			}
		}

		By byDir  = By.xpath("//*[@class=\"inline\" and contains(text(),'Dir'  )]/following-sibling::*");
		List<WebElement>   dirList   = driver.findElements(byDir);

		for (WebElement   ele1   : dirList) {
			String actDir = ele1.getText();
			if (actDir.contains(expDir)) {
				System.out.println("Found Dir : "+ actDir );
				break;

			}
		}

	}

 

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
