package day4;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex07_IMDB {

	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);

		driver.get("https://www.imdb.com/");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]")).sendKeys("Avengers");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search-button\"]")).click();

		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();


		//*[@class="inline" and contains(text(),'Star'  )]/following-sibling::*    All Stars' siblings

		By byStar  = By.xpath("//*[@class=\"inline\" and contains(text(),'Star'  )]/following-sibling::*");
		List<WebElement>   starList   = driver.findElements(byStar);


		String expStar = "Chris Evans";  //Shah Rukh Khan
		for (WebElement   ele   : starList) {
			String actStar = ele.getText();
			if (actStar.contains(expStar)) {
				System.out.println("Found Star : "+ actStar );
				break;

			}
		}
			By byDir  = By.xpath("//*[@class=\"inline\" and contains(text(),'Dir'  )]/following-sibling::*");
			List<WebElement>   dirList   = driver.findElements(byDir);


			String expDir = "Joss Whedon";  
			for (WebElement   ele1   : starList) {
				String actDir = ele1.getText();
				if (actDir.contains(expDir)) {
					System.out.println("Found Dir : "+ actDir );
					break;

				}
			}



		}




	}

