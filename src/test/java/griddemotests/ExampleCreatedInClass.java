package griddemotests;

import org.testng.annotations.Test;

import utils.HelperFunctions;
import utils.RemoteBrowserCreation;

import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class ExampleCreatedInClass {


	WebDriver driver;

	@BeforeTest
	public void beforeTest() throws MalformedURLException {

		driver = RemoteBrowserCreation.createRemoteDriver("firefox", "http://101.53.156.197/", "4456");

	}


	@Test
	public void f() {

		driver.get("https://www.google.com/");

		By bySearchT = By.name("q");

		WebElement elementSearch = driver.findElement(bySearchT);

		elementSearch.clear();

		elementSearch.sendKeys("CPSAT July 2020");

		elementSearch.sendKeys(Keys.ENTER);
		HelperFunctions.captureScreenShot(driver, "src/test/resources/google20.png");
		
		System.out.println("The TiTLE is : " + driver.getTitle());
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		


	}

	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}
