package byTeam;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class imdb_Ajit {
	/*
	 *
	 * Ex07 : IMDB Exercise

TASK#1
1. Go To https://www.imdb.com/

2. Search for movie Avengers

3. Click on magnifying glass

4. On new page click on the first link for Avengers

5. On the next page confirm
Stars: Chris Evans
Director: Joss Whedon
	 * */

	public static void main(String[] args) {

		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);

		driver.get("https://www.imdb.com/");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]")).sendKeys("Avengers");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search-button\"]")).click();

		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();

		String Dir=driver.findElement(By.xpath("//*[@id='title-overview-widget']/div[2]/div[1]/div[2]/h4")).getText();
		if (Dir.contains("Director") &&
				driver.findElement(By.xpath("//*[@id=\"title-overview-widget\"]/div[2]/div[1]/div[2]/h4//following::a[1]")).getText().contains("Joss Whedon"))
			System.out.println("The Director is Joss Whedon");

		else
			System.out.println("Element not present..");

		String Stars=driver.findElement(By.xpath("//*[@id='title-overview-widget']/div[2]/div[1]/div[4]/h4")).getText();

		//Check Chris Evans is present in nodes of Stars

		List <WebElement> cast= driver.findElements(By.xpath("//*[@id='title-overview-widget']/div[2]/div[1]/div[4]/h4//following-sibling::a"));
		for(WebElement ele : cast) {
			String str = ele.getText();
			if (str.contains("Chris Evans") && Stars.contains("Stars"))
				break;

		}

		System.out.println("Stars contains Chris Evans");  



	}

}