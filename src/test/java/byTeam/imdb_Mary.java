package byTeam;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import junit.framework.Assert;

class imdb_Mary {
	WebDriver Driver = null ;

	@BeforeEach
	void setUp() throws Exception {

		//Go To https://www.imdb.com/
		//open the chrome driver 
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		Driver = new ChromeDriver ();
		Driver.manage().window().maximize();
		Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Sets the imdb url

	



	}

	@AfterEach
	void tearDown() throws Exception {


		Driver.quit();
	}

	@Test
	void test() {
		Driver.get("https://www.imdb.com/");
		System.out.println("The page title is "+Driver.getTitle());
		/*By ByVar1 = By.xpath("//*[@id=\"suggestion-search\"]");
		WebElement WeSearch1 = Driver.findElement(ByVar1);
		WeSearch1.sendKeys("Avengers");*/
		
		//Search for movie Avengers
		//By ByVar1 = By.xpath("//*[@id=\"suggestion-search\"]");
		
		//By ByVar1 = By.id("suggestion-search");
		
		By ByVar1 = By.name("q");
		
		
		WebElement WeSearch1 = Driver.findElement(ByVar1);
		WeSearch1.sendKeys("Avengers");

		//Magnifying glass

		By ByVar2 = By.xpath("//*[@id=\"suggestion-search-button\"]");
		//*[@id="suggestion-search-button"]/svg
		//*[@id="suggestion-search-button"]/svg
		//*[@id="suggestion-search-button"]
		WebElement WeSearch2 = Driver.findElement(ByVar2);
		WeSearch2.click();
		System.out.println(Driver.getTitle());


		//On new page click on the first link for Avengers
		//*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a
		//*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a
		By ByVar3 = By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a");
		//Thread.sleep(6000);
		WebElement WeSearch3 = Driver.findElement(ByVar3);
		WeSearch3.click();
		//Thread.sleep(6000);
		System.out.println(Driver.getTitle());

		//On the next page confirm
		//Stars: Chris Evans
		//Director: Joss Whedon

		//*[@id="title-overview-widget"]/div[2]/div[1]/div[4]
		//*[@id="title-overview-widget"]/div[2]/div[1]/div[4]/a[1]
		//*[@id="title-overview-widget"]/div[2]/div[1]/div[4]/a[2]
		//*[@id="title-overview-widget"]/div[2]/div[1]/div[4]/a[3]
		//*[@id="title-overview-widget"]/div[2]/div[1]/div[4]/a[4]

		//*[@id="title-overview-widget"]/div[2]/div[1]/div[4]/a
		By ByVar4 = By.xpath("//*[@id=\"title-overview-widget\"]/div[2]/div[1]/div[4]");
		//Thread.sleep(6000);


		WebElement WeSearch5 = Driver.findElement(ByVar4);
		String SActual = WeSearch5.getText();
		System.out.println(SActual);
		if ( SActual.contains("Stars")) {

			System.out.println ("The expected text Stars is found ");
		}



		if ( SActual.contains("Chris Evans")) {

			System.out.println ("The expected text Chris Evans is found ");
		}



		//Director: Joss Whedon
		//*[@id="title-overview-widget"]/div[2]/div[1]/div[2]

		WebElement sElement = Driver.findElement(By.xpath("//*[@id=\"title-overview-widget\"]/div[2]/div[1]/div[2]"));
		String sText = sElement.getText();

		if (sText.contains ("Director")) {
			System.out.println ("The text contains Director");}


		if (sText.contains ("Joss Whedon")) {
			System.out.println ("The text contains Joss Whedon");}









	}
}
