package zref;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ex11_JQuery_AutoCompleteAndIFrame_Java {

	public static void main(String[] args) throws InterruptedException {

		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://jqueryui.com/autocomplete/");
		
		
		// To handle the frame (Using by index)
		driver.switchTo().frame(0);
		WebElement element = driver.findElement(By.className("ui-autocomplete-input"));
		element.sendKeys("j");
		
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("/html/body/ul/li")));
		
		
		List<WebElement> lst = driver.findElements(By.xpath("/html/body/ul/li"));
		for(int i=0; i < lst.size(); i++){
			//WebElement ele = driver.findElement(By.xpath("/html/body/ul/li["+i+"]"));
			WebElement ele = lst.get(i);
			System.out.println(ele.getText());
			if(ele.getText().equals("Java")){
				System.out.println(i);
				ele.click();
				Thread.sleep(3000);
			}
		}
	}
}
