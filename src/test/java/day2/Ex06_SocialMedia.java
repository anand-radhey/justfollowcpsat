package day2;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Ex06_SocialMedia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
//		ChromeOptions chromeOptions = new ChromeOptions();
//		chromeOptions.setHeadless(true);
//				
//		WebDriver driver = new ChromeDriver(chromeOptions);
//		
		
		
		//System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
		//WebDriver driver = new FirefoxDriver();
		
		//driver.manage().window().maximize();
		
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);
		
		
		
		driver.get("http://agiletestingalliance.org/");
		
		String filename = "src\\test\\resources\\screenshots\\imageSocialMedia5.png";
		
		utils.HelperFunctions.captureScreenShot(driver, filename);
		
		
		//*[@id="custom_html-10"]/div/ul/li[1]/a/i    LinkedIn
		//*[@id="custom_html-10"]/div/ul/li[2]/a/i    Twitter
		//*[@id="custom_html-10"]/div/ul/li[3]/a/i    YouTube
		//*[@id="custom_html-10"]/div/ul/li[4]/a/i    Insta
		//*[@id="custom_html-10"]/div/ul/li[5]/a/i    Facebook
		//*[@id="custom_html-10"]/div/ul/li[6]/a/i    WA
		//*[@id="custom_html-10"]/div/ul/li[7]/a/i    Telegram
		
		//*[@id="custom_html-10"]/div/ul/li[*]/a
		//*[@id="custom_html-10"]/div/ul/li/a     7 elements
		By byvar = By.xpath("//*[@id=\"custom_html-10\"]/div/ul/li/a");
		
		List<WebElement> welst   = driver.findElements(byvar);
		
		for(WebElement    ele    : welst) {
			String hrefstr = ele.getAttribute("href");
			System.out.println("Href is : "+ hrefstr);		
		}
		
		driver.quit();

	}

}
