package day3;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex02B_WindowSwitching {

	public static void main(String[] args) {
		
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("firefox", false);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.get("https://www.ataevents.org/");
		
		
		
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/figure/a/img   img1
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/figure/a/img   image2
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[3]/div/div/div/div/div/figure/a/img
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[4]/div/div/div/div/div/figure/a/img
		
		By byvar = By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[*]/div/div/div/div/div/figure/a");
		List<WebElement> welinkList = driver.findElements(byvar);
		
		for(WebElement    ele     : welinkList) {
			ele.click();
			//System.out.println("Page Title : "+ driver.getTitle());
			//System.out.println("Window Handle : "+ driver.getWindowHandle());
		}
		
		Set<String> setwinhandles  =    driver.getWindowHandles();
		for(  String   winhandle  :  setwinhandles) {
			System.out.println("Window Handle "+ winhandle);
			
			driver.switchTo().window(winhandle);
			System.out.println("Page Title : "+ driver.getTitle());
			
		}
		
		
		
		//driver.close();
		driver.quit();
		
		
		
		
		

	}

}
