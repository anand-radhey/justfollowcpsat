package day2;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex01_GoogleNavigation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		
//		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
//		WebDriver driver = new ChromeDriver();
//		driver.manage().window().maximize();
//		
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome", false);
		
		driver.get("https://www.google.com/");
		
		String filename1 = "src\\test\\resources\\screenshots\\imagePage1_01.png";
		String filename2 = "src\\test\\resources\\screenshots\\imagePage2_01.png";
		String filename3 = "src\\test\\resources\\screenshots\\imagePage3_01.png";
		
		
		utils.HelperFunctions.captureScreenShot(driver, filename1);
		
		String actPage1 = driver.getTitle();
		System.out.println("Title Page1 : "+  actPage1  );
		
		String expPage1 = "Google";
		if ( actPage1 == expPage1) {
			System.out.println("Correct Page1 Title ");
		} else {
			System.out.println("INCorrect Page1 Title ");
		}
		
		
		//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input   searchboax
		
		//By byvar = By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input");
		By byvar = By.xpath("//*[@name='q']");
		//By byvar = By.name("q");
		
		WebElement weSearch   = driver.findElement(byvar);
		
		weSearch.sendKeys("CPSAT");
		
		weSearch.sendKeys(Keys.ENTER);
		
		utils.HelperFunctions.captureScreenShot(driver, filename2);
		
		//weSearch.sendKeys(Keys.ENTER);
		
		System.out.println("Title Page2 : "+    driver.getTitle());
		
		//*[@id="rso"]/div[1]/div/div[1]/a/h3    FirstLink
		
		
		
		By byvar2 = By.xpath("//*[@id=\"rso\"]/div[1]/div/div[1]/a/h3");
		
		WebElement welink = driver.findElement(byvar2);
		welink.click();
		
		utils.HelperFunctions.captureScreenShot(driver, filename3);
		
		System.out.println("Title Page3 : "+    driver.getTitle());
		
		driver.navigate().back();
		System.out.println("Title Page3-back = Page2  : "+    driver.getTitle());
		
		driver.navigate().forward();
		System.out.println("Title Page3-back--forward = Page3  : "+    driver.getTitle());
		
		
		
		//driver.close();
		driver.quit();
		
		
		
		
		
		
		
		
		
		

	}

}
