package griddemotests;

import org.testng.annotations.Test;

import utils.HelperFunctions;
import utils.RemoteBrowserCreation;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

public class RemoteATAExample {

	WebDriver driver;

	@Parameters({"browser","host","port"})
	@BeforeTest
	public void beforeTest(@Optional("chrome") String browser,
			@Optional("http://101.53.156.197") String host, 
			@Optional("5555")String port) throws MalformedURLException, InterruptedException {
		//driver = BrowserCreation.createRemoteDriver("chrome", "http://164.52.193.178", "4444");
		
		System.out.println("browser =" + browser);
		System.out.println("host =" + host);
		System.out.println("port =" + port);

		/*
			some times testng optional parameters - gives extra "quotes"
			// we can try removing extra quotes
		 */
		
		
		browser = browser.replace("\"", "");
		host = host.replace("\"", "");
		port = port.replace("\"", "");

		System.out.println("browser =" + browser);
		System.out.println("host =" + host);
		System.out.println("port =" + port);

		System.out.println("creating remote webdriver");
		
		driver = RemoteBrowserCreation.createRemoteDriver(browser, host , port);	
		
		// RemoteWebDriver
		// RemoteWebDriver (URL of the hub or the server where the hub is there)
		
		//driver = new ChromeDriver();

	}


	@Test
	public void f() {

		driver.get("https://doppa2020.devopsppalliance.org/");
		
		// let us take a screenshot
		
		HelperFunctions.captureScreenShot(driver, "src/test/resources/doppasiteimage20.png");
		
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("All done exiting");

	}



	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}
