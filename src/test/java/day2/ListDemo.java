package day2;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> lstInt = new ArrayList<Integer>();
		lstInt.add(100);
		lstInt.add(300);
		
		
		List<String> lstStr = new ArrayList<String>();
		lstStr.add("India");
		lstStr.add("Japan");
		lstStr.add("USA");
		lstStr.add("UK");
		
		int cnt = lstStr.size();
		
		
		for (int i=0; i < cnt ; i++ ) {
			System.out.println(lstStr.get(i));
		}
		
		for ( String ele     : lstStr) {
			System.out.println(ele);
		}

	}

}
