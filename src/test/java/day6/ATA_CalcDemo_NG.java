package day6;
//POM
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class ATA_CalcDemo_NG {
	WebDriver driver = null;
	
	
	
	  @Test
	  public void f_add() {
		  
		  driver.get("http://ata123456789123456789.appspot.com/");
		  
		//*[@id="ID_nameField1"]
		  
		//*[@id="ID_nameField2"]
		  
		//*[@id="gwt-uid-2"]
		  
		//*[@id="gwt-uid-1"]
		  
		//*[@id="ID_calculator"]
		  
		//*[@id="ID_nameField3"]
		  
		  By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
		  By byField2  = By.xpath("//*[@id=\"ID_nameField2\"]");
		  
		  By byAdd  = By.xpath("//*[@id=\"gwt-uid-1\"]");
		  By byMul  = By.xpath("//*[@id=\"gwt-uid-2\"]");
		  By byCalc  = By.xpath("//*[@id=\"ID_calculator\"]");
		  By byResult  = By.xpath("//*[@id=\"ID_nameField3\"]");
		  
		  
		  WebElement weField1 = driver.findElement(byField1);
		  WebElement weField2 = driver.findElement(byField2);
		  WebElement weAdd = driver.findElement(byAdd);
		  WebElement weMul = driver.findElement(byMul);
		  WebElement weCalc = driver.findElement(byCalc);
		  WebElement weResult = driver.findElement(byResult);
		
		  
		  weField1.sendKeys("100");
		  weField2.sendKeys("22");
		  weAdd.click();
		  weCalc.click();
		  String result1 = weResult.getAttribute("value");
		  Assert.assertEquals(result1, "122");
	
	  }

	
	
	
  @Test
  public void f_mul() {
	  
	  driver.get("http://ata123456789123456789.appspot.com/");
	  
	//*[@id="ID_nameField1"]
	  
	//*[@id="ID_nameField2"]
	  
	//*[@id="gwt-uid-2"]
	  
	//*[@id="gwt-uid-1"]
	  
	//*[@id="ID_calculator"]
	  
	//*[@id="ID_nameField3"]
	  
	  By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
	  By byField2  = By.xpath("//*[@id=\"ID_nameField2\"]");
	  
	  By byAdd  = By.xpath("//*[@id=\"gwt-uid-1\"]");
	  By byMul  = By.xpath("//*[@id=\"gwt-uid-2\"]");
	  By byCalc  = By.xpath("//*[@id=\"ID_calculator\"]");
	  By byResult  = By.xpath("//*[@id=\"ID_nameField3\"]");
	  
	  
	  WebElement weField1 = driver.findElement(byField1);
	  WebElement weField2 = driver.findElement(byField2);
	  WebElement weAdd = driver.findElement(byAdd);
	  WebElement weMul = driver.findElement(byMul);
	  WebElement weCalc = driver.findElement(byCalc);
	  WebElement weResult = driver.findElement(byResult);
	  
	  weField1.sendKeys("10");
	  weField2.sendKeys("20");
	  weMul.click();
	  weCalc.click();
	  String result = weResult.getAttribute("value");
	  Assert.assertEquals(result, "200");
	  
  }
  
  @BeforeTest
  public void beforeTest() {
	  driver = utils.HelperFunctions.createAppropriateDriver("chrome");
	  
  }

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
